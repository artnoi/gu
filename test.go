package main

import (
	"os"
	"fmt"
)

func read(path string) []os.DirEntry {
	dir, err := os.ReadDir(path)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	return dir
}

func main() {

	entries := read("/home/artnoi")

	var names []string
	var sizes []int64
	var dirs []os.DirEntry
	var loop func([]os.DirEntry) ([]string, []int64, []os.DirEntry)

	loop =   func([]os.DirEntry) ([]string, []int64, []os.DirEntry) {

		for _, e := range entries {
			info, err := e.Info()
			if err != nil {
				panic(err)
			}
			if info.IsDir() {
				fmt.Printf("%s is dir\n", info.Name())
				dirs = append (dirs, e)
			}
			names = append(names, info.Name())
			sizes = append(sizes, info.Size())
		}
		return names, sizes, dirs
	}

	names, sizes, dirs = loop(entries)
	names, sizes, _ = loop(dirs)
	fmt.Println(names, sizes)
}
