# gu - Go clone of UNIX du(1)
gu is a very small and limited program to display file size in bytes. It also sorts the files in a given directory in bytes, or just output the size for single files. Because the output is sorted by file size, largest file is output last, which is useful because I tend to look for largest files in a given directory.

Unlike the original `du(1)`, gu does not support any command-line flags or options, and it won't support displaying the size in units other than bytes, because you can do just that with `du(1)` or any other programs.
