package main

import (
	"os"
	"fmt"
	"sort"
)

/* pair and pairs for sorting */
type pair struct {
	key string
	val int64	
}
/* Define pairs as slice of pair(s), and implement Sort for pairs */
type pairs []pair
func (p pairs) Len() int { return len(p) }
func (p pairs) Less(i, j int) bool { return p[i].val < p[j].val }
func (p pairs) Swap(i, j int) { p[i], p[j] = p[j], p[i] }

func sortMap(m map[string]int64) pairs {

	/* len(m) is number of arguments (files/dirs) */
	p := make(pairs, len(m))
	i := 0
	/* Copy file/size 'pair' into pairs */
	for k, v := range m {
		p[i] = pair{k, v}
		i++
	}
	/* Sort needs Len(), Less(), and Swap() */
	sort.Sort(p)
	return p
}

func mapNameSize(entries []os.DirEntry) map[string]int64 {
	var fmap = make(map[string]int64)
	for _, e := range entries {
		finfo, _ := e.Info()
		fmap[finfo.Name()] = finfo.Size()
	}
	return fmap
}

func main() {

	var dir []string
	if len(os.Args) > 1 {
		/* Get dir from argument */
		dir = os.Args[1:]
	} else {
		/* Else work in pwd */
		dir = append(dir, ".")
	}

	var sorted = pairs{}
	/* Loop over args */
	for _, d := range dir {
		entries, err := os.ReadDir(d)
		if err != nil {
			/* If argument points to single file,
			 * os.ReadDir will return error.
			 * So we skip the first error from ReadDir,
			 * but will panic for error from os.Stat() below
			 */
			fp, err := os.Stat(d)
			if err != nil {
				fmt.Println(err)
				panic(err)
			}
			/* Single files are automatically sorted */
			sorted = pairs{pair{key: d, val: fp.Size()}}
		} else {
			/* If args are dirs, then sort */
			sorted = sortMap(mapNameSize(entries))
		}
		for _, v := range sorted {
			fmt.Printf("%-12v %v\n", v.val, v.key)
		}
	}
}
